package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Equipage;
import util.Context;

public class DAOEquipage {

			
		public void insert(Equipage e) {
			EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
	        
	        
	        
	        em.getTransaction().begin();
	        
	        
	        em.persist(e);
	        
	        
	        em.getTransaction().commit();
	        em.close();
	        Context.destroy();
		}
		
		public Equipage selectById(int id) {
			
			EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
	        
			Equipage e = em.find(Equipage.class, id);
	        System.out.println(e);
	        em.close();
	        Context.destroy();
			return e;
		}
		
		public List<Equipage> selectAll() {
			
			EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
	        
	        Query query =  em.createQuery("from Equipage");
	        List<Equipage> equipages = query.getResultList();
	       
	        em.close();
	        Context.destroy();
			return equipages;
		}
		
		public void update(Equipage e) {
			EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
	        
	        
	        
	        em.getTransaction().begin();
	        
	        em.merge(e);
	        
	       
	        
	        
	        //em.persist(a);
	        
	        
	        em.getTransaction().commit();
	        em.close();
	        Context.destroy();
		}
		
		public void delete(Equipage e) {
			EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
	        
	        
	        
	        em.getTransaction().begin();
	        
	        
	        e=em.merge(e);
	        em.remove(e);
	        
	       
	        
	        
	        //em.persist(a);
	        
	        
	        em.getTransaction().commit();
	        em.close();
	        Context.destroy();
		}
	

	
	
	
	
	
	
}
