package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Planete;
import util.Context;

public class DAOPlanete {
	
	public void insert(Planete p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

        em.getTransaction().begin();
 
        em.persist(p);

        em.getTransaction().commit();
        em.close();
        Context.destroy();
	}
	
	public Planete selectById(int id) {
		
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
        
        Planete p = em.find(Planete.class, id);
        System.out.println(p);
        em.close();
        Context.destroy();
		return p;
	}
	
	public List<Planete> selectAll() {
		
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
        
        Query query =  em.createQuery("from Planete");
        List<Planete> Planetes = query.getResultList();
       
        em.close();
        Context.destroy();
		return Planetes;
	}
	
	public void update(Planete p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

        em.getTransaction().begin();
        
        em.merge(p);

        //em.persist(a);

        em.getTransaction().commit();
        em.close();
        Context.destroy();
	}
	
	public void delete(Planete p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

        em.getTransaction().begin();

        p=em.merge(p);
        em.remove(p);

        //em.persist(p);
   
        em.getTransaction().commit();
        em.close();
        Context.destroy();
	}
}
