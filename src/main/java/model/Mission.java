package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="mission")
public class Mission {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private int id;
	
	@Version
	private int version;
	
	@Column(name="libelle", nullable=false)
	private String libelle;
	
	@Column(name="niveau")
	private int niveau;
	
	@Column(name="recompense")
	private int recompense;
	
	@Column(name="quete", nullable=false)
	private Planete quete;

	public Mission(int id, String libelle, int niveau, int recompense, Planete quete) {
		super();
		this.id = id;
	
		this.libelle = libelle;
		this.niveau = niveau;
		this.recompense = recompense;
		this.quete = quete;
	}
	
	public Mission(String libelle, int niveau, int recompense, Planete quete) {
		super();
		this.libelle = libelle;
		this.niveau = niveau;
		this.recompense = recompense;
		this.quete = quete;
	}
	
	public Mission() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

	public int getRecompense() {
		return recompense;
	}

	public void setRecompense(int recompense) {
		this.recompense = recompense;
	}

	public Planete getQuete() {
		return quete;
	}

	public void setQuete(Planete quete) {
		this.quete = quete;
	}

	@Override
	public String toString() {
		return "Mission [libelle=" + libelle + ", niveau=" + niveau + ", recompense=" + recompense + ", quete=" + quete
				+ "]";
	}
	
	
	
	
}
