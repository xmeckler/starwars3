package model;

public class Vaisseau {

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name="Vaisseau")
public class Vaisseau {
	
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="id", nullable=false)
		private int id;
		
		@Column(name="name", nullable=false)
		private String nom;
		
		@Column(name="couleur")
		private String couleur;
		
//		@OneToOne(mappedBy="vaisseau")
//		private Equipage equipage;
		
		@Version
		private int version;

		public Vaisseau(String nom, String couleur) {
			this.nom = nom;
			this.couleur = couleur;
		}
		
		public Vaisseau() {
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getCouleur() {
			return couleur;
		}

		public void setCouleur(String couleur) {
			this.couleur = couleur;
		}

		public int getVersion() {
			return version;
		}

		public void setVersion(int version) {
			this.version = version;
		}

		@Override
		public String toString() {
			return "Vaisseau [nom=" + nom + ", couleur=" + couleur + "]";
		}


}
