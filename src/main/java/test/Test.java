package test;

import javax.persistence.EntityManagerFactory;


import util.Context;

import java.util.List;

import javax.persistence.*;

public class Test {

	public static void main(String[] args) {
		
		//EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
		
		//System.out.println(em.find(Film.class, 1));
		

		//em.find(Film.class, 1);
		
		//Context.destroy();

		Context.destroy();

		//generateBdd();
		
	}
	
	public static void generateBdd() {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		/*
		Acteur ac1 = new Acteur("Lablague", "Toto");
		Acteur ac2 = new Acteur("Staline", "Silvester");
		Acteur ac3 = new Acteur("Lauricella", "Christopher");
		
		Agent a1 = new Agent("Blino", "Yoan");
		Agent a2 = new Agent("Lauricella", "ChrisChris");
		Agent a3 = new Agent("Bouteloup", "Remix");
		
		Producteur p1 = new Producteur("Abid","Jordan");
		Producteur p2 = new Producteur("Montarou","Alexis");
		
		ac1.setAgent(a1);
		ac2.setAgent(a2);
		ac3.setAgent(a3);
		
		
		
		Film f1 = new Film("Toto sur un tracteur", 280);
		f1.setType(Type.Horreur);
		f1.setProducteur(p2);
		f1.addActeur(ac1);
		f1.addActeur(ac2);
		
		Film f2 = new Film("Toto fait des saltos", 360);
		f2.setType(Type.Comique);
		f2.setProducteur(p2);
		f2.addActeur(ac1);
		
		Film f3 = new Film("Toto le mentaliste",30);
		f3.setProducteur(p1);
		f3.setType(Type.Comique);
		f3.addActeur(ac1);
		f3.addActeur(ac2);
		f3.addActeur(ac3);
		
		
		em.persist(f1);
		em.persist(f2);
		em.persist(f3);
		*/
		em.getTransaction().commit();
		Context.destroy();

	}
	
	

}
